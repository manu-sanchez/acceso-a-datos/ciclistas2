<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    // CONSULTA 1
    public function actionConsulta1a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("COUNT(*) numero_ciclistas")->distinct(),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['numero_ciclistas'],
           "titulo"=>"Consulta 1 con Active Record",
           "enunciado"=>"Número de ciclistas que hay.",
           "sql"=>"SELECT DISTINCT COUNT(*) numero_ciclistas FROM ciclista",
        ]); 
    }
    
    public function actionConsulta1 () {
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('select count(distinct edad) from ciclista')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT COUNT(*) numero_ciclistas FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay.",
            "sql"=>"SELECT DISTINCT COUNT(*) numero_ciclistas FROM ciclista",
        ]);
    }
    
    
    // CONSULTA 2
    public function actionConsulta2a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("COUNT(*) numero_ciclistas_Banesto")
                ->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas_Banesto'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto.",
            "sql"=>"SELECT DISTINCT COUNT(*) numero_ciclistas FROM ciclista WHERE nomequipo='Banesto'",
        ]); 
    }
    
    public function actionConsulta2 () {
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('SELECT DISTINCT COUNT(*) numero_ciclistas_Banesto FROM ciclista WHERE nomequipo="Banesto"')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT COUNT(*) numero_ciclistas_Banesto FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas_Banesto'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto.",
            "sql"=>"SELECT DISTINCT COUNT(*) numero_ciclistas FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    
    // CONSULTA 3
    public function actionConsulta3a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("AVG(edad) edad_media")->distinct(),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>"SELECT DISTINCT AVG(edad) FROM ciclista",
        ]); 
    }
    
    public function actionConsulta3 () {
        //Mediante DAO
        $numero=Yii::$app->db   //Misma consulta que utiliza Yii para contar con Active Record
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT AVG(edad) FROM ciclista) c')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT AVG(edad) edad_media FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>"SELECT DISTINCT AVG(edad) FROM ciclista",
        ]);
    }
    
    
    // CONSULTA 4
    public function actionConsulta4a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("AVG(edad) edad_media")
                ->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Edad media de los ciclistas de Banesto.",
            "sql"=>"SELECT DISTINCT AVG(edad) FROM ciclista WHERE nomequipo='Banesto'",
        ]); 
    }
    
    public function actionConsulta4 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT AVG(edad) FROM ciclista WHERE nomequipo="Banesto") c')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT AVG(edad) edad_media FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Edad media de los ciclistas de Banesto.",
            "sql"=>"SELECT DISTINCT AVG(edad) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    
    // CONSULTA 5
    public function actionConsulta5a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("nomequipo,AVG(edad) edad_media")
                ->distinct()->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Edad media de los ciclistas por cada equipo.",
            "sql"=>"SELECT DISTINCT nomequipo,AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]); 
    }
    
    public function actionConsulta5 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT nomequipo, AVG(edad) AS edad_media '
                        . 'FROM ciclista GROUP BY nomequipo) c')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT nomequipo,AVG(edad) edad_media FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Edad media de los ciclistas por cada equipo.",
            "sql"=>"SELECT DISTINCT AVG(edad) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    
    // CONSULTA 6
    public function actionConsulta6a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("nomequipo,COUNT(*) numero_ciclistas")
                ->distinct()->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_ciclistas'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Número de ciclistas por equipo.",
            "sql"=>"SELECT DISTINCT nomequipo,COUNT(*) FROM ciclista GROUP BY nomequipo",
        ]); 
    }
    
    public function actionConsulta6 () { 
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('SELECT COUNT(*) FROM '
                . '(SELECT DISTINCT nomequipo, COUNT(*) AS numero_ciclistas FROM ciclista GROUP BY nomequipo) c')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT nomequipo,AVG(edad) edad_media FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Número de ciclistas por equipo.",
            "sql"=>"SELECT DISTINCT nomequipo,AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    
    // CONSULTA 7
    public function actionConsulta7a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Puerto::find()->select("COUNT(*) numero_puertos")->distinct(),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_puertos'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos.",
            "sql"=>"SELECT DISTINCT COUNT(*) numero_puertos FROM puerto",
        ]); 
    }
    
    public function actionConsulta7 () { 
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT COUNT(*) FROM puerto) c')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT COUNT(*) numero_puertos FROM puerto',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos.",
            "sql"=>"SELECT DISTINCT COUNT(*) numero_puertos FROM puerto",
        ]);
    }
    
    
    // CONSULTA 8
    public function actionConsulta8a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Puerto::find()->select("COUNT(*) numero_puertos")
                ->distinct()->where("altura>1500"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_puertos'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500.",
            "sql"=>"SELECT DISTINCT COUNT(*) numero_puertos FROM puerto WHERE altura>1500",
        ]); 
    }
    
    public function actionConsulta8 () { 
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT COUNT(*) '
                        . 'FROM puerto WHERE altura>1500) c')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT COUNT(*) numero_puertos FROM puerto WHERE altura>1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500.",
            "sql"=>"SELECT DISTINCT COUNT(*) numero_puertos FROM puerto WHERE altura>1500",
        ]);
    }
    
    
    // CONSULTA 9
    public function actionConsulta9a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("nomequipo,COUNT(*) numero_ciclistas")
                ->distinct()->groupBy("nomequipo")
                ->having("numero_ciclistas>4"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_ciclistas'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas.",
            "sql"=>"SELECT DISTINCT nomequipo,COUNT(*) numero_ciclistas FROM ciclista "
            . "GROUP BY nomequipo HAVING numero_ciclistas>4",
        ]); 
    }
    
    public function actionConsulta9 () { 
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT nomequipo,COUNT(*) numero_ciclistas '
                        . 'FROM ciclista GROUP BY nomequipo HAVING numero_ciclistas>4) c')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT nomequipo,COUNT(*) numero_ciclistas FROM ciclista '
            . 'GROUP BY nomequipo HAVING numero_ciclistas>4;',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_ciclistas'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas.",
            "sql"=>"SELECT DISTINCT nomequipo,COUNT(*) numero_ciclistas FROM ciclista "
            . "GROUP BY nomequipo HAVING numero_ciclistas>4",
        ]);
    }
    
    
    // CONSULTA 10
    public function actionConsulta10a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("nomequipo")
                ->distinct()->groupBy("nomequipo")
                ->having("COUNT(*)>4 AND AVG(edad) BETWEEN 28 AND 32"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.",
            "sql"=>"SELECT DISTINCT nomequipo FROM ciclista "
            . "GROUP BY nomequipo HAVING COUNT(*)>4 AND AVG(edad) BETWEEN 28 AND 32;",
        ]); 
    }
    
    public function actionConsulta10 () { 
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT nomequipo FROM ciclista GROUP BY nomequipo '
                        . 'HAVING COUNT(*)>4 AND AVG(edad) BETWEEN 28 AND 32) c')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4 AND AVG(edad) BETWEEN 28 AND 32',            
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.",
            "sql"=>"SELECT DISTINCT nomequipo FROM ciclista "
            . "GROUP BY nomequipo HAVING COUNT(*)>4 AND AVG(edad) BETWEEN 28 AND 32;",
        ]);
    }
    
    
    // CONSULTA 11
    public function actionConsulta11a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Etapa::find()->select("dorsal,COUNT(*) etapas_ganadas")
                ->distinct()->groupBy("dorsal"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas_ganadas'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas.",
            "sql"=>"SELECT dorsal,COUNT(*) etapas_ganadas FROM etapa GROUP BY dorsal",
        ]); 
    }
    
    public function actionConsulta11 () { 
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) '
                        . 'FROM (SELECT DISTINCT dorsal, COUNT(*) etapas_ganadas '
                        . 'FROM etapa GROUP BY dorsal) c')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT dorsal,COUNT(*) etapas_ganadas FROM etapa GROUP BY dorsal',            
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas_ganadas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas.",
            "sql"=>"SELECT dorsal,COUNT(*) etapas_ganadas FROM etapa GROUP BY dorsal",
        ]);
    }
    
    
    // CONSULTA 12
    public function actionConsulta12a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Etapa::find()->select("dorsal,COUNT(*) etapas_ganadas")
                ->distinct()->groupBy("dorsal")->having("etapas_ganadas>1"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas_ganadas'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa.",
            "sql"=>"SELECT dorsal,COUNT(*) AS etapas_ganadas FROM etapa GROUP BY dorsal HAVING etapas_ganadas>1",
        ]); 
    }
    
    public function actionConsulta12 () { 
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT DISTINCT dorsal,COUNT(*) etapas_ganadas '
                        . 'FROM etapa GROUP BY dorsal HAVING etapas_ganadas>1) c')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT dorsal,COUNT(*) etapas_ganadas FROM etapa GROUP BY dorsal  HAVING etapas_ganadas>1',            
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas_ganadas'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa.",
            "sql"=>"SELECT dorsal,COUNT(*) AS etapas_ganadas FROM etapa GROUP BY dorsal HAVING etapas_ganadas>1",
        ]);
    }
    
    
}
