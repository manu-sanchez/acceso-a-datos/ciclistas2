<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Models\Lleva */

$this->title = $model->numetapa;
$this->params['breadcrumbs'][] = ['label' => 'Llevas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lleva-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'numetapa' => $model->numetapa, 'código' => $model->código], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'numetapa' => $model->numetapa, 'código' => $model->código], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dorsal',
            'numetapa',
            'código',
        ],
    ]) ?>

</div>
